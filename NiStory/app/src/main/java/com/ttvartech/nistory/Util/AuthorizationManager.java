package com.ttvartech.nistory.Util;
//
//import com.htec.internship.data.models.User;
//import com.htec.internship.utils.CashingManager;

import static com.ttvartech.nistory.Util.Constants.*;
public class AuthorizationManager {

    public static Boolean getIsActive() {
        return CashingManager.load().readBool(IS_ACTIVE);
    }

    public static void setIsActive(Boolean val) {
        CashingManager.load().store(IS_ACTIVE, val);
    }

    public static String getUserName() {
        return CashingManager.load().readString(USER_NAME);
    }

    public static void setUserName(String val) {
        CashingManager.load().store(USER_NAME, val);
    }

    public static String getUserPassword() {
        return CashingManager.load().readString(USER_PASSWORD);
    }

    public static void setUserPassword(String val) {
        CashingManager.load().store(USER_PASSWORD, val);
    }

    public static String getUserUid() {
        return CashingManager.load().readString(USER_UID);
    }

    public static void setUserUid(String val) {
        CashingManager.load().store(USER_UID, val);
    }

}
