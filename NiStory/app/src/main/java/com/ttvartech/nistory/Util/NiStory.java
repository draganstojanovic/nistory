package com.ttvartech.nistory.Util;

import android.app.Application;
import android.content.Context;

public class NiStory extends Application {
    public static NiStory INSTANCE;

    public static Context getContext() {
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }
}
