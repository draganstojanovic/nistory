package com.ttvartech.nistory.Util;

import com.ttvartech.nistory.User;

public class Constants {
    public static final String IS_ACTIVE = "IS_ACTIVE";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_PASSWORD = "USER_PASSWORD";
    public static final String USER_UID = "USER_UID";
}
