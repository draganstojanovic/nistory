package com.ttvartech.nistory;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ttvartech.nistory.Util.AuthorizationManager;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddNewStoryActivity extends AppCompatActivity {

    int oldPoints = 0;

    //Add new story
    private ImageView mNewStoryImageImageView;
    private ImageButton mNewStoryImageImageButton;
    private ImageButton mNewStoryLocationImageButton;
    private EditText mNewStoryDescriptionEditText;
    private View mProgressView;
    private TextView mLocationTextView;

    //Firebase
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseRef;
    private StorageReference mStorageRef;
    FirebaseUser currentUser;

    static final int REQUEST_TAKE_IMAGE = 1;
    Uri mFirebaseStoryImageUrl;
    String imageFileName;
    String mCurrentPhotoPath;
    Uri imageUriInStorage;

    //Maps location
    private GoogleMap mMap;
    private boolean mLocationPermissionGranted = false;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;

    private double mStoryLatitude;
    private double mStoryLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_story);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Finding views in activity_add_new_story
        mNewStoryImageImageView = findViewById(R.id.new_story_image_image_view);
        mNewStoryImageImageButton = findViewById(R.id.add_new_story_image_image_button);
        mNewStoryLocationImageButton = findViewById(R.id.add_new_story_location_image_button);
        mNewStoryDescriptionEditText = findViewById(R.id.new_story_description_edit_text);
        mProgressView = findViewById(R.id.story_image_upload_progress_bar);
        mLocationTextView = findViewById(R.id.location_text_view);

        //Firebase
        mAuth = FirebaseAuth.getInstance();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        currentUser = mAuth.getCurrentUser();

        getLocationPermission();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    public void takeStoryImage(View view) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_IMAGE);
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        galleryAddImage();
        setPic();
    }

    private void setPic() {
        mNewStoryImageImageButton.setVisibility(View.GONE);
        Glide.with(getApplicationContext()).load(mCurrentPhotoPath).into(mNewStoryImageImageView);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddImage() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        imageUriInStorage = Uri.fromFile(f);
        mediaScanIntent.setData(imageUriInStorage);
        this.sendBroadcast(mediaScanIntent);
    }

    public void setStoryLocation(View view) {

        Toast.makeText(AddNewStoryActivity.this, "Getting new location!", Toast.LENGTH_SHORT).show();

        GetLastKnownLocation();

    }

    private void storePhotoInDatabase() {

        StorageReference riversRef = mStorageRef.child("user_story_photos/" + imageFileName);

        riversRef.putFile(imageUriInStorage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // Get a URL to the uploaded content
                mFirebaseStoryImageUrl = taskSnapshot.getDownloadUrl();

                createStory();

            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {

                        Toast.makeText(AddNewStoryActivity.this, "Upload failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }



    private void createStory() {
        String mStoryID = currentUser.getUid();
        String mStoryUserName = currentUser.getDisplayName();
        String mStoryUserImage = currentUser.getPhotoUrl().toString();
        String mStoryTime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String mStoryDescription = mNewStoryDescriptionEditText.getText().toString();
        String mStoryImageUrl = mFirebaseStoryImageUrl.toString();
        int mStoryHearts = 0;

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Story story = new Story(mStoryUserImage, mStoryUserName, mStoryTime, mStoryDescription, mStoryImageUrl, mStoryHearts, mStoryLatitude, mStoryLongitude);
        ref.child("stories/" + mStoryID).child("story_" + mStoryTime).setValue(story);


        //Update points
        onStoryCreatedIncrementUserPoints(mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()));

        Toast.makeText(AddNewStoryActivity.this, "Story created", Toast.LENGTH_SHORT).show();



    }

    private void onStoryCreatedIncrementUserPoints(DatabaseReference postRef) {

        postRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                oldPoints = user.getmUserPoints();

                mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).child("mUserPoints").setValue(oldPoints + 1);

                mProgressView.setVisibility(View.GONE);

                finish();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_new_story_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.publish_story_item) {
            mProgressView.setVisibility(View.VISIBLE);
            storePhotoInDatabase();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }


    }

    public void GetLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object

                            mLocationTextView.setText(location.getLatitude() + " " + location.getLongitude());

                            mStoryLatitude = location.getLatitude();
                            mStoryLongitude = location.getLongitude();

                        }
                    }
                });
    }

}
