package com.ttvartech.nistory;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class MonumentActivity extends AppCompatActivity {

    ImageView imageView;
    TextView monumentNameTextView;
    TextView monumentHeartsTextView;
    TextView monumentDescriptionTextView;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monument);

        imageView = findViewById(R.id.monument_image_image_view);
        monumentNameTextView = findViewById(R.id.monument_name_text_view);
        monumentHeartsTextView = findViewById(R.id.monument_hearts_text_view);
        monumentDescriptionTextView = findViewById(R.id.monument_description_text_view);

        Intent intent = getIntent();

        Monument monument = (Monument) intent.getSerializableExtra("selectedMonument");

        Glide.with(getApplicationContext()).load(monument.getmMonumentImageUrl_1()).into(imageView);

        monumentNameTextView.setText(monument.getmMonumentName());
        monumentHeartsTextView.setText("" + monument.getmMonumentHearts());
        monumentDescriptionTextView.setText(monument.getmMonumentDescription());
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MonumentActivity.this, AugmentedRealityActivity.class);
                intent.putExtra("monumentAR", monument);
                startActivity(intent);

            }
        });

    }
}
