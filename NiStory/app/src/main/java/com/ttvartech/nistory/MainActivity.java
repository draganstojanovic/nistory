package com.ttvartech.nistory;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.ttvartech.nistory.Util.AuthorizationManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.ttvartech.nistory.AddMonuments.addMonuments;

import static com.ttvartech.nistory.FirebaseDB.addMonumentInUser;
import static com.ttvartech.nistory.FirebaseDB.addUserInStory;
import static com.ttvartech.nistory.FirebaseDB.deleteMonumentInUser;
import static com.ttvartech.nistory.FirebaseDB.deleteUserInStory;
import static com.ttvartech.nistory.FirebaseDB.getMonuments;
import static com.ttvartech.nistory.FirebaseDB.getMonumentsForSpecificUser;
import static com.ttvartech.nistory.FirebaseDB.updateMonuments;
import static com.ttvartech.nistory.FirebaseDB.updateStories;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private static ArrayList<Story> storyArrayList = new ArrayList<>();
    private static ArrayList<String> storiesForCurrentUser = new ArrayList<>();
    private static ArrayList<Monument> monumentArrayList;
    private static ArrayList<User> friendArrayList;
    private static ArrayList<String> monumentsForCurrentUser;
    private static ArrayList<String> likedStoriesIDArrayList = new ArrayList<>();

    private static ArrayList<Location> monumentLocations;

    private static boolean locationServiceRunning = false;
    private static int checked;


    private boolean mLocationPermissionGranted = false;

    private FusedLocationProviderClient mFusedLocationClient;

    private LocationCallback mLocationCallback;

    private LocationRequest mLocationRequest;

    private DatabaseReference mDatabaseRef;
    private LatLng myPosition;


    /////////////////////////fragment
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    /////////////////////////fragment

    private FirebaseAuth mAuth;
    private static FloatingActionButton fab;
    private int selectedTab;
    private static int mActiveFragment;
    private String mCurrentUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        mAuth = FirebaseAuth.getInstance();

        tryToSignIn();

        getLocationPermission();
        getLocation();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, AddNewStoryActivity.class);
                startActivity(intent);

//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mActiveFragment = tab.getPosition();
                animateFab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));




        FirebaseUser firebaseUser = mAuth.getCurrentUser();


        // UserToken.USER_UID = firebaseUser.getUid();

        ImageView imageView = navigationView.getHeaderView(0).findViewById(R.id.nav_user_image_image_view);
        Glide.with(getApplicationContext()).load(firebaseUser.getPhotoUrl()).apply(RequestOptions.circleCropTransform()).into(imageView);
        TextView nameTextView = navigationView.getHeaderView(0).findViewById(R.id.nav_header_user_first_and_last_name);
        nameTextView.setText(firebaseUser.getDisplayName());
        TextView emailTextView = navigationView.getHeaderView(0).findViewById(R.id.nav_header_user_email);
        emailTextView.setText(firebaseUser.getEmail());


    }

    private void tryToSignIn() {
        mAuth.signInWithEmailAndPassword(AuthorizationManager.getUserName(), AuthorizationManager.getUserPassword())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser user;
                            user = mAuth.getCurrentUser();
                            AuthorizationManager.setUserUid(user.getUid());

                            Toast.makeText(MainActivity.this, "Authentication succeeded.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }


    private void animateFab(int position) {
        switch (position) {
            case 0: {
                fab.setVisibility(View.VISIBLE);
                selectedTab = 0;
                invalidateOptionsMenu();
            }

            break;
            case 1: {
                fab.setVisibility(View.GONE);
                selectedTab = 1;
                invalidateOptionsMenu();
            }

            break;

            default: {
                fab.setVisibility(View.GONE);
                selectedTab = 2;
                invalidateOptionsMenu();
            }

            break;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        //Firebase
        private FirebaseAuth mAuth;
        private DatabaseReference mDatabaseRef;
        private StorageReference mStorageRef;
        FirebaseUser currentUser;

        public ArrayList<String> userFriendsIDArrayList;

        //private ArrayList<Story> storyArrayList;
        StoryAdapter mStoryAdapter;

        //private ArrayList<Monument> monumentArrayList;
        MonumentAdapter mMonumentAdapter;

        //private ArrayList<User> friendArrayList;
        FriendAdapter mFriendAdapter;

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";


        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            //Firebase
            mAuth = FirebaseAuth.getInstance();
            mDatabaseRef = FirebaseDatabase.getInstance().getReference();
            mStorageRef = FirebaseStorage.getInstance().getReference();

            userFriendsIDArrayList = new ArrayList<>();

            friendArrayList = new ArrayList<>();

            monumentLocations = new ArrayList<>();


            View rootView = null;
            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1: {

                    rootView = inflater.inflate(R.layout.fragment_stories, container, false);

                    RecyclerView mStoriesRecyclerView = rootView.findViewById(R.id.stories_recycler_view);
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                    mStoriesRecyclerView.setLayoutManager(mLayoutManager);
                    mStoryAdapter = new StoryAdapter(storyArrayList, likedStoriesIDArrayList, new StoryAdapter.Listener() {
                        @Override
                        public void onItemClick(Story story) {
                            //Toast.makeText(getContext(), story.getmStoryUserName(), Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void like(Story story, int pos) {
                            // Toast.makeText(getContext(), "like", Toast.LENGTH_LONG).show();
                            updateStories(story, "like", pos);
                            addUserInStory(story, pos, storyArrayList);

                        }

                        @Override
                        public void dislike(Story story, int pos) {
                            //Toast.makeText(getContext(), story.getmStoryUserName(), Toast.LENGTH_LONG).show();
                            updateStories(story, "dislike", pos);
                            deleteUserInStory(story);

                        }
                    });
                    mStoriesRecyclerView.setAdapter(mStoryAdapter);
                    getFriendsStories();
                    break;
                }
                case 2: {
                    monumentArrayList = new ArrayList<>();
                    monumentsForCurrentUser = new ArrayList<>();
                    rootView = inflater.inflate(R.layout.fragment_monuments, container, false);

                    RecyclerView mMonumentsRecyclerView = rootView.findViewById(R.id.monuments_recycler_view);
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                    mMonumentsRecyclerView.setLayoutManager(mLayoutManager);
                    mMonumentAdapter = new MonumentAdapter(monumentArrayList, monumentsForCurrentUser, new MonumentAdapter.Listener() {
                        @Override
                        public void onItemClick(Monument monument) {
                            Intent intent = new Intent(getContext(), MonumentActivity.class);
                            intent.putExtra("selectedMonument", monument);
                            startActivity(intent);
                        }

                        @Override
                        public void like(Monument monument) {
                            updateMonuments(monument, "like");
                            addMonumentInUser(monument);
                        }

                        @Override
                        public void dislike(Monument monument) {
                            updateMonuments(monument, "dislike");
                            deleteMonumentInUser(monument);
                        }
                    });
                    mMonumentsRecyclerView.setAdapter(mMonumentAdapter);

                    getMonuments(monumentArrayList, monumentLocations, mMonumentAdapter);
                    getMonumentsForSpecificUser(monumentsForCurrentUser, mMonumentAdapter);

                    break;
                }
                case 3: {

                    rootView = inflater.inflate(R.layout.fragment_friends, container, false);

                    RecyclerView mFriendsRecyclerView = rootView.findViewById(R.id.friends_recycler_view);
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                    mFriendsRecyclerView.setLayoutManager(mLayoutManager);
                    mFriendAdapter = new FriendAdapter(friendArrayList);
                    mFriendsRecyclerView.setAdapter(mFriendAdapter);

                    getFriends();

                    // what goes here?
                    break;
                }
            }


            return rootView;

        }

        public void getFriendsStories() {
            //Get datasnapshot at your "users" root node
            final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(AuthorizationManager.getUserUid()).child("friends");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {

                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        String s = postSnapshot.getValue(String.class);
                        userFriendsIDArrayList.add(s);
                    }

                    getStories();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        public void getStories() {
            storyArrayList.clear();
            likedStoriesIDArrayList.clear();
            for (final String friend : userFriendsIDArrayList) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("stories").child(friend);
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        Log.e("Count ", "" + snapshot.getChildrenCount());
                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {


                            Story story = postSnapshot.getValue(Story.class);
                            story.setmStoryID(postSnapshot.getKey());
                            story.setmStoryCreatorID(friend);


                            int found = -1;
                            int index = 0;

                            for (Story story1 : storyArrayList) {
                                if (story1.getmStoryImageUrl().equals(story.getmStoryImageUrl()))
                                    found = index;

                                index++;
                            }

                            storyUsers(story, index);

                            if (found < 0)
                                storyArrayList.add(story);
                            else
                                storyArrayList.set(found, story);


                        }

                        Collections.sort(storyArrayList);
                        mStoryAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        }

        private void storyUsers(final Story story, final int pos) {
            DatabaseReference ref = FirebaseDatabase.getInstance()
                    .getReference()
                    .child("stories")
                    .child(story.getmStoryCreatorID())
                    .child(story.getmStoryID())
                    .child("users");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean isLiked = false;
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        String user = postSnapshot.getValue(String.class);
                        if (user.equals(AuthorizationManager.getUserUid())) {
                            int found = -1;
                            int index = 0;

                            for (String s : likedStoriesIDArrayList) {
                                if (s.equals(story.getmStoryID()))
                                    found = index;
                                index++;
                            }

                            if (found < 0)
                                likedStoriesIDArrayList.add(story.getmStoryID());
                            else
                                likedStoriesIDArrayList.set(found, story.getmStoryID());

                            isLiked = true;
                        }
                    }

                    if (!isLiked) {
                        int pos = 0;

                        // Log.e("------------------------------------> likedStoriesIDArrayList <------------------------------------- ", "" + likedStoriesIDArrayList.size());
//                        for (String s : likedStoriesIDArrayList) {
//                            if (s.equals(story.getmStoryID()))
//                                likedStoriesIDArrayList.remove(s);
//                            pos++;
//                        }

                        Iterator<String> iterator = likedStoriesIDArrayList.iterator();

                        //Step 4: Iterate Using while loop
                        int index = 0;
                        while (iterator.hasNext()) {
                            //System.out.println(iterator.next());
                            Log.e("---------> iterator.hasNext() <--------- ", "" + iterator.next());
                            Log.e("---------> iterator.hasNext() <---------", "" + iterator);
                            if (likedStoriesIDArrayList.get(index).equals(story.getmStoryID()))
                                likedStoriesIDArrayList.remove(story.getmStoryID());
                            index++;
                        }
                    }
                    mStoryAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        }

        public void getFriends() {


            //Get datasnapshot at your "users" root node
            final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(AuthorizationManager.getUserUid()).child("friends");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {

                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        String s = postSnapshot.getValue(String.class);
                        userFriendsIDArrayList.add(s);
                    }
                    getEveryFriend();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        public void getEveryFriend() {

            friendArrayList.clear();

            for (final String friend : userFriendsIDArrayList) {


                //Get datasnapshot at your "users" root node
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users").child(friend);

                ref.addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        Log.e("Count ", "" + snapshot.getChildrenCount());

                        User user = snapshot.getValue(User.class);

                        int found = -1;
                        int index = 0;

                        for (User user1 : friendArrayList) {
                            if (user1.getmUserEmail().equals(user.getmUserEmail()))
                                found = index;

                            index++;
                        }

                        if (found < 0)
                            friendArrayList.add(user);
                        else
                            friendArrayList.set(found, user);

                        Collections.sort(friendArrayList);

                        mFriendAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // if (selectedTab == 2)
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_map_view) {


            switch (mActiveFragment) {
                case 0: {

                    Intent intent = new Intent(MainActivity.this, MarkerMapsActivity.class);
                    intent.putExtra("fragment", mActiveFragment);
                    intent.putExtra("stories", storyArrayList);
                    startActivity(intent);

                    break;
                }
                case 1: {

                    Intent intent = new Intent(MainActivity.this, MarkerMapsActivity.class);
                    intent.putExtra("fragment", mActiveFragment);
                    intent.putExtra("monuments", monumentArrayList);
                    startActivity(intent);

                    break;
                }
                case 2: {

                    Intent intent = new Intent(MainActivity.this, FriendMapsActivity.class);
                    intent.putExtra("friends", friendArrayList);
                    startActivity(intent);

                    break;
                }

            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_track_changes) {
            showLocationServiceDialog();
        } else if (id == R.id.nav_my_location) {
            Intent intent = new Intent(MainActivity.this, MyLocationMapsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_my_profile) {
            Intent intent = new Intent(MainActivity.this, UserActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_add_friend) {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(MainActivity.this, BluetoothFriendActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            addMonuments();
            //addFieldInMonument();
            //addFieldInUser();
        } else if (id == R.id.nav_about_us) {
            Intent intent = new Intent(MainActivity.this, AboutUsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_sign_out) {
            mAuth.getInstance().signOut();
            AuthorizationManager.setUserName("");
            AuthorizationManager.setUserPassword("");
            AuthorizationManager.setUserUid("");
            Intent intent = new Intent(MainActivity.this, SignInActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void showLocationServiceDialog() {

        locationServiceRunning = isMyServiceRunning(MyService.class);

        if (locationServiceRunning)
            checked = 0;
        else
            checked = 1;

        final CharSequence[] charSequence = new CharSequence[]{"On", "Off"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Locator")
                //.setMessage("You can buy our products without registration too. Enjoy the shopping")
                .setSingleChoiceItems(charSequence, checked, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // locationServiceRunning = !locationServiceRunning;

                        if (which == 0) {
                            checked = 0;
                            locationServiceRunning = true;
                        } else {
                            checked = 1;
                            locationServiceRunning = false;
                        }


                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(MainActivity.this, MyService.class);
                        startService(intent);

                        if (locationServiceRunning) {
                            Toast.makeText(getApplicationContext(), "Locator is On", Toast.LENGTH_SHORT).show();
                            intent.putExtra("monuments", monumentArrayList);
                            startService(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Locator is Off", Toast.LENGTH_SHORT).show();
                            stopService(intent);
                        }


                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    @Override
    public void onStart() {
        super.onStart();

        // Check if user is signed in (non-null) and update UI accordingly.

        return;
    }

    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void getLocation() {

        mDatabaseRef = FirebaseDatabase.getInstance().getReference();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationRequest = createLocationRequest();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    // ...
                    //Toast.makeText(getApplicationContext(), "Location:" + location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_SHORT).show();

                    mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).child("mUserLatitude").setValue(location.getLatitude());
                    mDatabaseRef.child("users").child(AuthorizationManager.getUserUid()).child("mUserLongitude").setValue(location.getLongitude());
                }
            }


        };
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(300000); //5min
        mLocationRequest.setFastestInterval(300000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        return mLocationRequest;
    }

    boolean mRequestingLocationUpdates = true;

    @Override
    protected void onResume() {
        super.onResume();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

}


